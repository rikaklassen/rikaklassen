# To Do List
- Create GitLab Pages
- Create document explaining why content are not on certain platforms
- Mirror Instagram photos to Pixelfed and Calckey / Misskey
- Back up Tumblr posts
- Back up tweets
- Figure out if it's worth changing liscence to a different Creative Commons

If you find want me to spend more time on the projects, please consider tipping via [PayPal](https://paypal.me/bglamours).

Last updated April 5, 2023 (18:44:06 UTC).
